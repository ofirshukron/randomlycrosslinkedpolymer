% startme 

if ispc
% compile the c++ code
[compileStat,compileMsgOut]= system('g++ -c ./Src/main.cpp -g -O1 -O -Os -O3 -O0 -O2 -std=c++14 -std=c++11 -Wall -o ./Build/main.cpp.o -L./Src/ -I./Src/')

% build executable
[buildStat,buildMsgOut] = system('g++ ./Build/main.cpp.o -o ./RCLsimulation.exe')
end