% compile and biold C++ project to simulate realization of the Randomly
% Cross-Linked polymer


if ispc
% Run simulations on Windows machine
% this is a sample code, with several parameters values set. The user may
% wish to chane this paramer values to suit his/her needs. Please follow
% the example bellow and refer to the user guide supplied with this
% simulation framework. 

numSteps           = 100;
numSimulations     = 20;
numRelaxationSteps = 10000;
numConnectors      = 20;
recordPosition     = 'false';
execString = sprintf('%s%s%d%s%d%s%d%s%s%s%d','RCLsimulation.exe',...
    ' -numSteps ',numSteps,...
    ' -numSimulations ', numSimulations,...
    ' -numRelaxationSteps ', numRelaxationSteps,...
    ' -recordPosition ', recordPosition,...
    ' -numConnectors ', numConnectors);
[runStat, runMsgOut] = system(execString)


elseif isunix
% unfinished
end