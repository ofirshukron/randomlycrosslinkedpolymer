using namespace std;
using namespace Eigen;


class RCLUtils
{
 public:
 
 RCLUtils()
 {
   // class constructor
  };
  
time_t Tic()
 {
    clock_t runTime   = clock();
    return runTime;
 }
  
int Toc(time_t tic)
 {
    
    printf("time: %f %s\n",(Tic()-(double)tic) / CLOCKS_PER_SEC," sec.");
    
    return 0;
}



string CombinePaths(string path1, string path2)
{
	// Combine two strings describing a path 
	string path;
	path = string(path1)+"/"+string(path2);
	return path;
}

 void createDir(string dir) {
     
#if defined(_WIN64)
mkdir(dir.data());
#else 
mkdir(dir.data(), 0777); // notice that 777 is different than 0777
#endif
     
//#if defined _MSC_VER
//    _mkdir(dir.data());
//#elif defined __GNUC__
//    mkdir(dir.data(),S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
//#endif
}
 
MatrixXf PairwiseDistance1(MatrixXf vec1, MatrixXf vec2)
{
    // vec1 and vec2 must have the same dimension (number of columns)
    MatrixXf dist = MatrixXf::Constant(vec1.rows(), vec2.rows(),0.0);   
    if  (vec1.rows()==vec2.rows())
    {
        dist = PairwiseDistance2(vec1,vec2);
    }
    else {
        for (int pIdx=0; pIdx<vec1.rows(); pIdx++)
        {
            for (int qIdx=0; qIdx<vec2.rows(); qIdx++)
            {
                dist(pIdx,qIdx) = (vec1.row(pIdx)-vec2.row(qIdx)).norm();
            }        
        }    
    }
    return dist;
}

MatrixXf  PairwiseDistance2(MatrixXf vec1, MatrixXf vec2)
{
   // vec1 and vec2 must have the same dimension (number of columns)   
    MatrixXf dist(vec1.rows(),vec2.rows());
    MatrixXf uu = vec1.rowwise().squaredNorm();
    MatrixXf vv = vec2.rowwise().squaredNorm();
    
    // this is the norm squared
    for (int pIdx=0; pIdx<vec1.rows(); pIdx++)
    {
        for (int qIdx = 0; qIdx<pIdx; qIdx++)
        {
          dist(pIdx,qIdx) = sqrt(uu(pIdx)+vv(qIdx)-2.0*vec1.row(pIdx).dot(vec2.row(qIdx)));
          dist(qIdx,pIdx) = dist(pIdx,qIdx);
        }
    }
	return dist;

}


};// end class 


//int constractUtils()
//{
//    RCLUtils utils;
//    return 0;
//};