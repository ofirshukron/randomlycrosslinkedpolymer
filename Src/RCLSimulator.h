// header file for the RCL simulation framework

# include <stdio.h>
# include <iostream>
# include <fstream>
# include <time.h>
# include <random>
# include <math.h>
# include <string>
#ifdef WINDOWS
	#include <direct.h>
    #define GetCurrentDir _getcwd
	#include <dirent.h>
    #include <windows.h>	
#else
    #include <unistd.h>
	#include <sys/types.h>
    #include <sys/stat.h>
    #define GetCurrentDir getcwd
 #endif
 
//# include "/usr/include/boost/random.hpp"
# include <./Eigen/Eigen>
//# include <boost/filesystem.hpp>
//# include <boost/format.hpp>

//# include "./Eigen/Eigen"
//# include  "D:/Ofir/ENS/TestFiles/code/C++/RCLPolymer/simulation/Eigen/Eigen"
#include "RandomlyCrossLinkedPolymer.cpp"
RandomlyCrossLinkedPolymer rcl; 

//# include "RCLUtils.cpp"

#include <RCLUtils.cpp>
RCLUtils utils; // Global 

#include "RCLResultHandler.cpp"
#include "ForceManager.cpp"

using namespace std;
using namespace Eigen;
