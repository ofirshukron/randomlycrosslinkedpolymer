# include <iostream>
# include <stdio.h>
# include <math.h>
//#include <RCLUtils.cpp>
//#include <RCLUtils.cpp>
using namespace std;
//using namespace boost::filesystem;
using namespace Eigen;
//RCLUtils utils;
class RCLResultHandler{
    public:
    
    struct pathsStruct{
      string resultFolder;
      string variance;
      string MSD;
      string msdFit;
      string meanShortestPath;
      string configuration;
      string encounter;
      string encounterFreq;
      string encounterProb;
      string source;
      string figures;
    };
    
    // running indices
    struct simCounters{
      int experiment;
      int simulation;
      int step;
    };
    
 struct resultStruct
{
    MatrixXd encounterTime;    // encounter time fetMonomer1 fetMonomer2
    MatrixXd srg;              // square radius of gyration
    MatrixXd msrg;             // mean square radius of gyration 
    MatrixXd mfet;             // mean first encounter time 
    MatrixXd msdSim;           // msd for each simulation
    MatrixXd msdExp;           // msd for each experiment
    MatrixXd encounterFreqMat; // encounter frequency matrix 
    MatrixXd encounterProbMat; // encounter probability matrix 
    MatrixXd varianceSim;      // monomer variance
};
    
    // initialize counter struct
    simCounters counter;

    // initialize resultStruct;
    resultStruct results;
    
		

    // initialize  properties
    pathsStruct paths;
    int experiment = 0;
    int simulation = 0;
    int step       = 0;
        
    int Initialize(string resultBaseFolderPath)
    {
		RCLUtils utils;
        // initialize directories paths 
        string resultFolderName=utils.CombinePaths(resultBaseFolderPath,"Result_Date");// input result base path
        utils.createDir(resultFolderName);// create result directory in base
        paths.resultFolder = resultFolderName;
        
        string variancePath=utils.CombinePaths(resultFolderName, "Variance");
        utils.createDir(variancePath);// create result directory in base        
        paths.variance = variancePath;        
        
        string msdPath=utils.CombinePaths(resultFolderName, "MSD");
        utils.createDir(msdPath);// create directory
        paths.MSD = msdPath;
        
        
        string msdFitPath=utils.CombinePaths(msdPath,"Fit");
        paths.msdFit = msdFitPath;
        utils.createDir(msdFitPath);// create directory
        
        string mspPath=utils.CombinePaths(resultFolderName, "MeanShortestPath");
        utils.createDir(mspPath);// create directory
        paths.meanShortestPath = mspPath;       
        
        
        string confPath=utils.CombinePaths(resultFolderName, "Configuration");
        utils.createDir(confPath);// create directory
        paths.configuration = confPath;
        
        string encounterPath=utils.CombinePaths(resultFolderName,"Encounter");
        utils.createDir(encounterPath);// create directory
        paths.encounter = encounterPath;
        
        string encounterFreqPath=utils.CombinePaths(encounterPath,"Frequency");
        utils.createDir(encounterFreqPath);// create directory
        paths.encounterFreq = encounterFreqPath;
        
        string encounterProbPath=utils.CombinePaths(encounterPath,"Probability");
        utils.createDir(encounterProbPath);// create directory
        paths.encounterProb = encounterProbPath;
        
        
        string sourcePath=utils.CombinePaths(resultFolderName,"Source");
        utils.createDir(sourcePath);// create directory
        paths.source = sourcePath;
        
        string figuresPath=utils.CombinePaths(resultFolderName,"Figures");
        utils.createDir(figuresPath);// create directory
        paths.figures= figuresPath;
                     

        return 1;
    }
    
    int Step()
    {
        counter.step+=1;
        // advance the step counter 
        return 0;        
    }
    
    int Simulation()
    {
        // advance the simulation counter, make preparation for new simulation 
        counter.simulation +=1;
        
        return 0;
    }
    
    int Experiment()
    {
        // Advance the experiment counter, make preparation for new experiment
        counter.experiment += 1;
       
        
//        InitializeResultsStruct();
        
        
        return 0;
    }
    
    
//  resultStruct InitializeResultStruct(paramStruct params)// should be moved to resultHandler
//{
//    resultStruct results;
//    results.encounterTime    = MatrixXd::Constant(params.numSimulations, params.numExperiments, 0.0); // first encounter time between fetMonomer1 and fetMonomer2
//    results.mfet             = MatrixXd::Constant(1,params.numExperiments, 0.0); // the mean first encounter time 
//    results.srg              = MatrixXd::Constant(params.numSimulations,params.numExperiments,0.0);// square radius of gyration 
//    results.msrg             = MatrixXd::Constant(1,params.numExperiments, 0.0); // the square radius of gyration simulation values
//    results.msdSim           = MatrixXd::Constant(params.numMSDSteps, 1, 0.0);    // msd for each simulation
//    results.msdExp           = MatrixXd::Constant(params.numMSDSteps, params.numSimulations, 0.0); // msd for each experiment
//    results.encounterFreqMat = MatrixXd::Constant(params.numMonomers,params.numMonomers,0.0); // encounter frequency matrix 
//    results.varianceSim      = MatrixXd::Constant(params.numMonomers,params.numMonomers,0.0);// monomer variance 
//    
//    return results;
//}
    
    int RecordMSD()
    {
     return 0;
    }
    
    int RecordSquareRadiusOfGyration(double srg)
    {
        
        return 0;
    }
        
    int CreateResultFolder(string resultBaseFolderPath)
    {     	
      string resultFolderName= resultBaseFolderPath;// input result base path 
      utils.createDir(resultFolderName);// create result directory in base
      string resultFileName =utils.CombinePaths(resultFolderName, "resultSummaryTest.csv");
      string encounterFileName=utils.CombinePaths(resultFolderName, "encounters.csv");
      string fetFolderPath=utils.CombinePaths(resultFolderName, "FirstEncounterTime/");
      
      // Create csv output file 
      std::ofstream resultSummary(resultFileName, std::ofstream::ate); // Opening file to print info to
      resultSummary << "experiment"<< ","<< "MSRGsimulation"<< ","<< "MSRGthreory"<< ","<< "MFETsimulation"
                  << ","<< "MFETtheory"<<","<< "fetMonomer1"<< ","<< "fetMonomer2"<<","<< "endToEndVector"
                  << ","<< "connectivity"<< ","<< "numConnectors"<< ","<< "b"<< ","<< "diffusionConst"
                  << ","<< "numMonomers"<< ","<< "dt"<< ","<< "dtRelaxation"<< ","<< "normalTransform"
                  << ","<< "numSimulations"<< ","<< "numSteps"<< ","<< "recordMSD"<< ","<< "numMSDSteps"
                  << ","<< "minMonomerDistance"<< ","<< "encounterDist" << endl; // Headings for file
      resultSummary.close();
    
      std::ofstream monomerEncountersFile(encounterFileName,std::ofstream::ate);
      monomerEncountersFile.close();
        
      return 0;
    }
    
    
    
};

int Construct()
{
    // constructor 
    RCLResultHandler resultHandler;     
    return 0;
};