#include <RCLSimulator.h>
#include <string> 
using namespace std;
// simulation parameters
struct paramStruct { 
        // default parameters 
		// TODO parameters related to the polymer should be taken from the polymer class 
		
        int   numExperiments      = 2;     // number of experiments   
        int   numSimulations      = 50;     // number of simulations in each experiment     
        int   numRelaxationSteps  =10000; // number of relaxation steps
        int   numSteps            = 100;           // number of steps in each simulation 
        int   numDamageSteps      = 0;     // number of steps in damage and protein accumulation stage 
        int   numRepairSteps      =0;     // steps of the repair stage 
        int   numMonomers         = 50;        // number of monomers
        int   dimension = 3;          // dimension
        float D =1.0;                  // diffusion constant
        float b = sqrt(3.0);                  // std of connector length
        float minMonomerDist = 0.0;     // minaml distance between monomers (unused)
        float dt           = 0.01;                 // simulation time step
        float dtRelaxation= 0.05;      // time step for polymer relaxation stage (unused)
        bool  normalTransform= false;  // perform normal transformation 
        int   numConnectors= 10;     // number of added connectors
		MatrixXf fixedConnectors; // indices of fixed connectors; 
        bool  transientSimulation = true; // perform transient (MFET) simulation?
        int   fetMonomer1=10;          // monomer 1 for FET
        int   fetMonomer2= 20;          // monomer 2 for FET
        float encounterDist= sqrt(3.0)/5.0;     // encounter distance
        int   maxFETsteps= 1e6;          // maximal number of FET simulation before termination
        bool  recordPosition= true;      // records polymer position at each step
        int   recordPositionInterval =10; // in terval in step to export position 
		bool  recordPositionDuringRelaxation = false; // record position of the chain in relxations 
		bool  recordPositionDuringFET = false; // record polymer position during first encounter time simulation 
		bool  recordPositionDuringMSD = false; // record polymer position dring MSD simulation 
        bool  recordMSD      = true;           // compute the MSD after relaxation stage?
        int   numMSDSteps    = 200;          // how many steps the MSD stage records
        float UVClaserRadius = 0.15;     // the radius of UVC laser (range of affect
        float connectivity =2.0*10.0/(49.0*48.0);            // connectivity fraction 
        bool  harmonicExclussionForce = false; 
        float harmonicExclussionForceMagnitude= -0.5; // spring force
        float harmonicExclussionForceLowerCutoff=0.0; // lower cutoff below which the force doesn't work
        float harmonicExclussionForceUpperCutoff=sqrt(3.0); // upper cutoff above which the force doesn't work
        bool  breakConnectorsFromDamagedMonomers =true; // after UVC break random connectors
//        MatrixXf laplacian;       // the Laplacian of the graph representation of the polymer
        string resultBaseFolder =utils.CombinePaths(GetCurrentDir(NULL,0),"Results");//"../../../../../OwnCloud/EpigenomicIntegrity/Tests/C++Code");;  // path to the result folder        
    };

// Absolute paths 
struct pathsStruct{
      string resultFolder;
      string resultSummary;
      string variance;
      string MSD;
	  string meanMSD;// over all monomers
      string msdFit;
      string meanShortestPath;
      string configuration;
      string encounter;
      string encounterFreq;
      string encounterProb;
      string source;
      string srg;
      string fet;
      string figures;
};

// file names
struct fileNameStruct{
    
  string resultSummary;
  string encounterFrequency;
  string encounterProb2Sides;
  string encounterProb1Side;
  string MSD;  // mean square displacement 
  string meanMSD; // over all monomers
  string fet; // first encounter time 
  string rog; // radius of gyration
  string variance; // variance
  string configuration; //polymer configuration
  string connectedMonomers; //polymer configuration
  string posXYZ; //polymer configuration
};

// Data structure for exeriments
struct experimentStruct{ 
    int                   expNum;
    int                   simNum;
    int                   stepNum;   
    MatrixXf         centerMass;        // chain center of mass 
    MatrixXf         connectedMonomers; // indices of connected monomers
    MatrixXf         damagedMonomers;   // indices of damaged monomers
    MatrixXf         laplacian;         // laplacian of the graph representation of the polymer
    float                roiRadius;//       // radius of the ROI 
    MatrixXf         encounterFreqMat;  // encounter frequency for all simulation in an experiment
    MatrixXf         encounterProbMat;    // encounter probability matrix 
    MatrixXf         encounterProb2Sides; // encounter prob mat
    MatrixXf         encounterProb1Side;  // one sided encounter prob 
    MatrixXf         encounterFreqMatPreUVC;//
    MatrixXf         encounterFreqMatPostUVC;//
    MatrixXf         monomerDist;// pairwise monomer distances
    MatrixXf         srg; //square radius of gyration
    MatrixXf         msrg; // mean square radius of gyration
    MatrixXf         mfet; // mean first encounter time 
    MatrixXf         fet;// first encounter time 
    MatrixXf         msd;// average msd over all simulations 
    MatrixXf         meanMSD; // average MSD over all monomers over all simulations
    MatrixXf         msdAlpha; // fit alpha anomalous exponent for each monomer 
    MatrixXf         msdBeta;// fir beta, bias for MSD
    
    paramStruct        params;
    pathsStruct          paths;
    fileNameStruct   fileNames;
    RandomlyCrossLinkedPolymer rcl;// the polymer class
	ForceManager     force; // force manager
    MatrixXf               varianceSim;// monomer variance
	MatrixXf               posX; // last Xposition in each simulation
    MatrixXf               posY; // last Y position in each simulation
    MatrixXf               posZ; // last position in each simulation    
	RCLUtils utils;
	
	int Initialize(paramStruct parameters){
		// initialize experimental structure 
	 params = parameters;
     expNum = 0;// experiment counter
	 simNum = 0;// simulation counter
     
     // initialize data structures collect data from all experiments
     mfet           = MatrixXf::Constant(1,params.numExperiments,0.0);
     msrg          = MatrixXf::Constant(1,params.numExperiments,0.0);
     msdAlpha = MatrixXf::Constant(params.numMonomers,params.numExperiments,0.0);
     msdBeta    = MatrixXf::Constant(params.numMonomers,params.numExperiments,0.0);
     
     CreateResultFolder();
     CreateResultSummaryFile();
    
	 InitializePolymer();// returns expStruct

	 return 0;
	}
	
	int NewExperiment(){
        expNum+=1;
        simNum  =0;

        encounterFreqMatPreUVC  = MatrixXf::Constant(rcl.params.numMonomers,rcl.params.numMonomers,0);
        encounterFreqMatPostUVC = MatrixXf::Constant(rcl.params.numMonomers,rcl.params.numMonomers,0);
        encounterFreqMat                = MatrixXf::Constant(rcl.params.numMonomers,rcl.params.numMonomers,0);
        varianceSim                           = MatrixXf::Constant(rcl.params.numMonomers,rcl.params.numMonomers,0.0);
        encounterProb2Sides           = MatrixXf::Constant(rcl.params.numMonomers,rcl.params.numMonomers,0.0); // encounter prob 2 sides matrix         
        encounterProbMat                = MatrixXf::Constant(rcl.params.numMonomers,rcl.params.numMonomers,0.0); // encounter prob 2 sides matrix         
        encounterProb1Side             = MatrixXf::Constant(rcl.params.numMonomers,rcl.params.numMonomers-1,0.0); // encounter prob 1 side matrix 
        fet                                            = MatrixXf::Constant(1,params.numSimulations,0.0);// first encounter time for each simulation
        srg                                           = MatrixXf::Constant(1,params.numSimulations,0.0);// square radius of gyration for each simulation
        if (params.recordMSD==true){
          msd                                         = MatrixXf::Constant(params.numMSDSteps,params.numMonomers,0.0);// mean  over monomers for each simulation 
          meanMSD                              = MatrixXf::Constant(params.numMSDSteps,params.numSimulations,0.0);// mean over al lmonomers over all simulations
        }
        // Preallocations for simulation last position data
        posX = MatrixXf::Constant(rcl.params.numMonomers, params.numSimulations,0.0);
        posY = MatrixXf::Constant(rcl.params.numMonomers, params.numSimulations,0.0);
        posZ = MatrixXf::Constant(rcl.params.numMonomers, params.numSimulations,0.0);

        // Prepare result csv files for the current experiment
        PrepareResultsCSVFiles();
        return 0;
	}

	int NewSimulation(){
		simNum +=1; // increase simulation counter by 1
        stepNum =0;// reset step counter
		InitializePolymer();
		return 0;
	}
	
	int InitializePolymer(){
     // Initialize polymer position, damaged monomers and connected monomer list
       
	 rcl.Initialize(params.numMonomers,
				   params.numConnectors,
				   params.fixedConnectors,
				   params.dimension,
				   params.D,    
				   params.b,
				   params.dt,
				   params.normalTransform);
    
     // Set no damaged monomers                                                   
     damagedMonomers= MatrixXf::Constant(params.numMonomers,1,0);// damaged
   
     return 0;
	}
	
	int CreateResultFolder(){        
	    // TODO create subfolders for the configuration of experiment and each simulation
	    // TODO move function to ResultHandler
        // initialize directories paths         
        // create directories if don't exist. save absolute paths in paths variable 
        std::stringstream folderDate;
        time_t now = time(0);
        tm *ltm      = localtime(&now);
        
        folderDate<<"SimulationResults_"<<ltm->tm_mday <<"_"<<ltm->tm_mon+1<<"_"<<ltm->tm_year+1900<<"_time_"<<ltm->tm_hour<<"_"<<ltm->tm_min;        
                
		string resultFolderName = utils.CombinePaths(params.resultBaseFolder,folderDate.str());// input result base path        		
        utils.createDir(resultFolderName);// create result directory in base
        paths.resultFolder = resultFolderName;
			
        string variancePath=utils.CombinePaths(resultFolderName,  "Variance");
        utils.createDir(variancePath);// create result directory in base        
        paths.variance = variancePath;        
        
        // square radius of gyration
        string srgPath=utils.CombinePaths(resultFolderName,  "RadiusOfGyration");
        utils.createDir(srgPath);// create result directory in base        
        paths.srg = srgPath;  
        
        // first encounter time
        string fetPath=utils.CombinePaths(resultFolderName,  "FirstEncounterTime");
        utils.createDir(fetPath);// create result directory in base        
        paths.fet = fetPath;
        
        string msdPath=utils.CombinePaths(resultFolderName,"MSD");
        utils.createDir(msdPath);// create directory
        paths.MSD = msdPath;
		
		string meanMsdPath=utils.CombinePaths(msdPath,"mean");
        utils.createDir(meanMsdPath);// create directory
        paths.meanMSD = meanMsdPath;

       
        string msdFitPath=utils.CombinePaths(msdPath,"Fit");        
        utils.createDir(msdFitPath);// create directory
        paths.msdFit = msdFitPath;
        
        string mspPath=utils.CombinePaths(resultFolderName,"MeanShortestPath");
        utils.createDir(mspPath);// create directory
        paths.meanShortestPath = mspPath;        
                
        string confPath=utils.CombinePaths(resultFolderName,"Configuration");
        utils.createDir(confPath);// create directory
        paths.configuration = confPath;
        
		// create a folder for each experiment and each simulation 
		
		string cFolderPath;
		std::stringstream fName1;
		std::stringstream fName2;		
			for (int expIdx=0; expIdx<params.numExperiments;expIdx++)
			{
				fName1.str("");
				fName1.clear();
				fName1<< "Exp"<<expIdx+1;
				for (int sIdx=0; sIdx<params.numSimulations;sIdx++)
				{  
					fName2.str("");
					fName2.clear();
			
					cFolderPath = utils.CombinePaths(paths.configuration,fName1.str());
					utils.createDir(cFolderPath);
					fName2 <<"Simulation"<<sIdx+1;
					cFolderPath = utils.CombinePaths(cFolderPath,fName2.str());
					utils.createDir(cFolderPath);
				}
			}		
            
        string encounterPath = utils.CombinePaths(resultFolderName,"Encounter");
        utils.createDir(encounterPath);// create directory
        paths.encounter = encounterPath;
        
        string encounterFreqPath=utils.CombinePaths(encounterPath,"Frequency");
        utils.createDir(encounterFreqPath);// create directory
        paths.encounterFreq = encounterFreqPath;
        
        string encounterProbPath=utils.CombinePaths(encounterPath,"Probability");
        utils.createDir(encounterProbPath);// create directory
        paths.encounterProb = encounterProbPath;
                
        string sourcePath=utils.CombinePaths(resultFolderName, "Source");
        utils.createDir(sourcePath);// create directory
        paths.source = sourcePath;
        
        string figuresPath=utils.CombinePaths(resultFolderName, "Figures");
        utils.createDir(figuresPath);// create directory
        paths.figures= figuresPath;
        
    return 0;
}
	
	int CreateResultSummaryFile(){
		string resultSummaryFileName=utils.CombinePaths(paths.resultFolder, "resultSummary.csv");// can be pased as pre made string      
		std::ofstream resultSummary(resultSummaryFileName, std::ofstream::ate);//  Opening file to print info to
		resultSummary << "experiment"<<","<< "MSRGsimulation"<< ","<< "MSRGtheory"<< ","<< "MFETsimulation"
					  << ","<< "MFETtheory"<<","<< "fetMonomer1"<< ","<< "fetMonomer2"<<","<< "endToEndVector"
					  << ","<< "connectivity"<< ","<< "numConnectors"<< ","<< "b"<< ","<< "diffusionConst"
					  << ","<< "numMonomers"<< ","<< "dt"<< ","<< "dtRelaxation"<< ","<< "normalTransform"
					  << ","<< "numSimulations"<< ","<< "numSteps"<< ","<< "recordMSD"<< ","<< "numMSDSteps"
					  << ","<< "minMonomerDistance"<< ","<< "encounterDist" << endl; // Headings for file
		resultSummary.close();
		
		paths.resultSummary = resultSummaryFileName;		
		return 0;
	}
	
	int PrepareResultsCSVFiles(){
        // Prepare encounter frequency csv file 
        std::stringstream ef2Sides;
        ef2Sides <<"Exp_"<<expNum<<"_encounterFrequency.csv";                            
        string encounterFreqFileName=utils.CombinePaths(paths.encounterFreq, ef2Sides.str());
        
        // update expStruct
        fileNames.encounterFrequency  = encounterFreqFileName;
        
        // Pointer
        std::ofstream encounterFreqFile(encounterFreqFileName,std::ofstream::ate);

        
        // prepare headers 
        for (int hIdx=0; hIdx<params.numMonomers;hIdx++)
        {                        
            encounterFreqFile<<"monomer"<< hIdx<<",";
        }
        encounterFreqFile<<endl;
        encounterFreqFile.close();
    
        // Prepare encounter probability two sides csv file
        std::stringstream ep2Sides; // file name 
        ep2Sides <<"Exp_"<<expNum<<"_encounterProb2Sided.csv";
        string encounterProb2SidedFileName=utils.CombinePaths(paths.encounterProb, ep2Sides.str()); // file path 
        
        // update expStruct
        fileNames.encounterProb2Sides  = encounterProb2SidedFileName;
        
        std::ofstream monomerEncountersProb2SidesFile(encounterProb2SidedFileName,std::ofstream::ate); // file pointer
        
        // prepare headers 
        for (int hIdx=0; hIdx<params.numMonomers;hIdx++)
        {                        
            monomerEncountersProb2SidesFile<<"monomer"<< hIdx+1<<",";
        }
        monomerEncountersProb2SidesFile<<endl;            
        monomerEncountersProb2SidesFile.close();

      // Prepare encounter probability 1 sided csv file 
         std::stringstream ep1SideFileName;
         ep1SideFileName <<"Exp_"<<expNum<<"_encounterProb1Side.csv";
         string encounterProb1SideFileName=utils.CombinePaths(paths.encounterProb, ep1SideFileName.str());
         
         // update expStuct
         fileNames.encounterProb1Side = encounterProb1SideFileName;
         
         std::ofstream monomerEncountersProb1SideFile(encounterProb1SideFileName,std::ofstream::ate);
         
         // prepare headers 
         for (int hIdx=0; hIdx<params.numMonomers;hIdx++)
         {                        
         monomerEncountersProb1SideFile<<"monomer"<< hIdx+1<<",";
         }
         monomerEncountersProb1SideFile<<endl; 
         monomerEncountersProb1SideFile.close();  
        
        // Prepare variance csv file 
        std::stringstream varianceFileName; 
        varianceFileName <<"Variance_Exp_"<<expNum<<"_simulation.csv"; // file name 
        string varianceFilePath=utils.CombinePaths(paths.variance, varianceFileName.str()); // file path 
        
        // save file name 
        fileNames.variance = varianceFilePath;        
        std::ofstream varianceFilePointer(varianceFilePath,std::ofstream::ate); // file pointer 

        // prepare headers 
        for (int hIdx=0; hIdx<params.numMonomers;hIdx++)
        {                        
            varianceFilePointer<<"monomer"<< hIdx+1<<",";
        }
        varianceFilePointer<<endl;
        varianceFilePointer.close();
        
        // Preparer mean MSD csv files
        std::stringstream meanMsdFileName; 
        meanMsdFileName <<"MSD_Exp_"<<expNum<<"_simulation.csv"; // file name 
        string meanMsdFilePath=utils.CombinePaths(paths.meanMSD, meanMsdFileName.str()); // file path 
        // save file name
        fileNames.meanMSD =meanMsdFilePath;
        
        std::ofstream meanMsdFilePointer(meanMsdFilePath,std::ofstream::ate); // file pointer 
        
        // prepare headers in the csv files 
        for (int hIdx=0; hIdx<params.numSimulations ;hIdx++)
        {                        
            meanMsdFilePointer<<"simulation_"<< hIdx+1<<",";
        }
        meanMsdFilePointer<<endl;            
        meanMsdFilePointer.close();   

       // Prepare MSD csv files
        std::stringstream msdFileName; 
        msdFileName <<"MSD_Exp_"<<expNum<<"_simulation.csv"; // file name 
        string msdFilePath=utils.CombinePaths(paths.MSD, msdFileName.str()); // file path 
        // save file name
        fileNames.MSD =msdFilePath;
        
        std::ofstream msdFilePointer(msdFilePath,std::ofstream::ate); // file pointer 
        
         //prepare headers in the csv files 
        for (int hIdx=0; hIdx<rcl.params.numMonomers;hIdx++)
        {                        // monomer header 
            msdFilePointer<<"monomer"<< hIdx+1<<",";
        }
        msdFilePointer<<endl; 
        msdFilePointer.close();   

        return 0;
        }

    int RecordLastPolymerPosition(){
        // Copy last polymer position for the computation of the variance 
            
            posX.col(simNum-1) = rcl.curPos.col(0);
            posY.col(simNum-1) = rcl.curPos.col(1);
            if (params.dimension==3)
            {
              posZ.col(simNum-1) = rcl.curPos.col(2);
            }
            else if (params.dimension==2)
            {
                posZ.col(simNum-1) = MatrixXf::Constant(rcl.params.numMonomers,1,0.0);
            }
        return 0;
    }
};

experimentStruct InduceUVC(experimentStruct& expStruct){
    // Shoot a cylindrical beam, all monomers in its pat up to radius of r will be damaged
    
    // a binary list with 1- damaged monomer, 0- undamaged
    // get monomer distance from polymer center of mass 
    MatrixXf cm  = expStruct.rcl.curPos.colwise().mean();
    
    // check the XY distance from cm
    MatrixXf distFromCm = utils.PairwiseDistance1(expStruct.rcl.curPos.block(0,0,expStruct.rcl.params.numMonomers,2),cm.block(0,0,1,2));
    
    for (int mIdx =0; mIdx<expStruct.rcl.params.numMonomers; mIdx++)
    {
        if( distFromCm(mIdx,0)<expStruct.params.UVClaserRadius)
        {
            expStruct.damagedMonomers(mIdx) = 1;// consider as damaged 
        }
    }
   
     MatrixXf cmod=expStruct.rcl.connectedMonomers; 
     
    // break all cross-links to and from damaged monomers
    if (expStruct.params.breakConnectorsFromDamagedMonomers)
    {       
      for (int mIdx=0; mIdx<cmod.rows(); mIdx++)
      {     
        
         if ((expStruct.damagedMonomers(cmod(mIdx,0))==1) ||  (expStruct.damagedMonomers(cmod(mIdx,1))==1))                          
              { // update the laplacian                     
               expStruct.rcl.DisconnectMonomers(cmod(mIdx,0),cmod(mIdx,1));// remove all random connectors
              }
      }
    }

    return expStruct;
}

/** Find encounters between monomers 
 * @brief find encounters between monomers 
 * @param pos polymer position 
 * @return encounter matrix 
 */
MatrixXf FindEncounters(experimentStruct& expStruct){
    float monomerDist =0.0;
    MatrixXf encounterMat=MatrixXf::Constant(expStruct.rcl.params.numMonomers, expStruct.rcl.params.numMonomers,0.0);
    MatrixXf posT        = expStruct.rcl.curPos;
    
    if (expStruct.rcl.params.normalTransform==true)
    {
        posT = expStruct.rcl.RouseEigenVectors(expStruct.rcl.params.numMonomers)*expStruct.rcl.curPos;
    }
    for (int mIdx=0; mIdx<expStruct.params.numMonomers; mIdx++)
    {
        for (int nIdx=mIdx+1; nIdx<expStruct.params.numMonomers; nIdx++)
        {
            monomerDist = 0.0;
            for (int dIdx=0; dIdx<expStruct.params.dimension; dIdx++)
            {
                monomerDist += pow(posT(mIdx,dIdx)-posT(nIdx,dIdx),2.0);
            }
            if (monomerDist<=pow(expStruct.params.encounterDist,2.0))
            {
              encounterMat(mIdx,nIdx) = 1.0;
              encounterMat(nIdx,mIdx) = 1.0;
            }
        }
    }
   
    return encounterMat;
}

int ExportConnectedMonomersList(experimentStruct& expStruct){
	    // export connected monomers 
	 std::stringstream connectedMonomersFileName; 
     connectedMonomersFileName <<"Exp"<<expStruct.expNum<<"/Simulation"<<expStruct.simNum<<"/connectedMonomers.csv"; // file name 
	 string connectedMonomersFilePath = utils.CombinePaths(expStruct.paths.configuration,connectedMonomersFileName.str());

		
    std::ofstream connectedMonomersPointer(connectedMonomersFilePath,std::ofstream::ate);
     for (int mIdx=0; mIdx<expStruct.rcl.connectedMonomers.rows(); mIdx++)
     {
         connectedMonomersPointer<<expStruct.rcl.connectedMonomers(mIdx,0)<<","
         <<expStruct.rcl.connectedMonomers(mIdx,1)<<endl;
     }
    
     connectedMonomersPointer.close();
	 return 0;
}

int ExportCurrentPosition(experimentStruct& expStruct){
    // TODO move function to ResultHandler or RCL
    // Export XYZ files read by chimera
  if (expStruct.stepNum % expStruct.params.recordPositionInterval ==  0)
  {
     std::stringstream posXYZFileName; 
     posXYZFileName <<"Exp"<<expStruct.expNum<<"/Simulation"<<expStruct.simNum<<"/step_"<<expStruct.rcl.stepNum<<".pdb"; // file name 
	 string posXYZFilePath = utils.CombinePaths(expStruct.paths.configuration,posXYZFileName.str());
   
            // export position PDB file 
                ///++++++++++++++++++++
        //# 1 -  6        Record name     "ATOM  "                                            
        //# 7 - 11        Integer          Atom serial number.                   
        //# 13 - 16        Atom            Atom name.                            
        //# 17             Character       Alternate location indicator.         
        //# 18 - 20        Residue name    Residue name.                         
        //# 22             Character       Chain identifier.                     
        //# 23 - 26        Integer         Residue sequence number.              
        //# 27             AChar           Code for insertion of residues.       
        //# 31 - 38        Real(8.3)       Orthogonal coordinates for X in Angstroms.                       
        //# 39 - 46        Real(8.3)       Orthogonal coordinates for Y in Angstroms.                            
        //# 47 - 54        Real(8.3)       Orthogonal coordinates for Z in Angstroms.                            
        //# 55 - 60        Real(6.2)       Occupancy.                            
        //# 61 - 66        Real(6.2)       Temperature factor (Default = 0.0).                   
        //# 73 - 76        LString(4)      Segment identifier, left-justified.   
        //# 77 - 78        LString(2)      Element symbol, right-justified.      
        //# 79 - 80        LString(2)      Charge on the atom.  
            
        // Example:
        //ATOM      1  TAD SOL 1   1      10.187  -6.452  -4.726  0.10  0.10          Cl  
        //ATOM      2  TAD SOL 1   2       9.952  -6.959  -4.013  0.10  0.10           P  
        //ATOM      3  TAD SOL 1   3       9.298  -7.455  -4.938  0.10  0.10           P  
        //ATOM      4  TAD SOL 1   4       8.393  -7.020  -5.532  0.10  0.10           P  

         // Create the list of off-diagonal connected monomers (additional random connectors)

    const char* connectorElement = "N"; // Nitrogen non damaged, not connected
    bool memberFlag         = false;
    int cIdx                = 0; // running index
	int f=0;
    FILE * pFile;                
    pFile = fopen(posXYZFilePath.c_str(),"w");
    
    float pos3 = 0.0; // third dimension in case of 2 dimensional simulation 
    
            for (int m1Idx=0; m1Idx<expStruct.params.numMonomers; m1Idx++)
            {
                connectorElement = "N";
                memberFlag       = false;
                cIdx             = 0;
                if (expStruct.rcl.connectedMonomers.rows()>0)
                    {
                      while (memberFlag==false){                                            
                        if ((expStruct.rcl.connectedMonomers(cIdx,0)==m1Idx) || (expStruct.rcl.connectedMonomers(cIdx,1)==m1Idx)){                        
                            // if monomer m1Idx appears in the pair 
                            if (abs(expStruct.rcl.connectedMonomers(cIdx,0)-expStruct.rcl.connectedMonomers(cIdx,1))>1){                            
                            connectorElement = "O";// Oxygen- element for the connected monomer non damaged
                            memberFlag        = true;  // out flag 
                            }
                         }                        
                        cIdx+=1;
                        
                        if (cIdx>=expStruct.rcl.connectedMonomers.rows()){            
                            memberFlag = true; // exit if all the list is scanned 
                        }
                     }
                 }                   
                           
                    if (expStruct.params.dimension ==3)
                    {
                        pos3 = expStruct.rcl.curPos(m1Idx,2);                        
                    }
                    else if (expStruct.params.dimension ==2)
                    {
                        pos3 = 0.0;
                    }
                    
             fprintf(pFile,"%-4.4s   %4.0d  %-2.2s  %3.3s %s%4.0d   %8.3f%8.3f%8.3f   %3.2f %2.2f           %1.1s\n",           
                           "ATOM",m1Idx+1,  connectorElement,    "HIS","A",1,     expStruct.rcl.curPos(m1Idx,0),expStruct.rcl.curPos(m1Idx,1),pos3,0.1, 0.1,connectorElement);
            } 
                    //
                    //# COLUMNS         DATA TYPE        FIELD           DEFINITION
                    //# ---------------------------------------------------------------------------------
                    //#  1 -  6         Record name      "CONECT"
                    //#  7 - 11         Integer          serial          Atom serial number
                    //# 12 - 16         Integer          serial          Serial number of bonded atom
                    //# 17 - 21         Integer          serial          Serial number of bonded atom
                    //# 22 - 26         Integer          serial          Serial number of bonded atom
                    //# 27 - 31         Integer          serial          Serial number of bonded atom
                    //# 32 - 36         Integer          serial          Serial number of hydrogen bonded atom                                                
                    //# 37 - 41         Integer          serial          Serial number of hydrogen bonded atom
                    //# 42 - 46         Integer          serial          Serial number of salt bridged atom
                    //# 47 - 51         Integer          serial          Serial number of hydrogen bonded atom 
                    //# 52 - 56         Integer          serial          Serial number of hydrogen bonded atom 
                    //# 57 - 61         Integer          serial          Serial number of salt bridged atom

        for (int m1Idx=0; m1Idx<expStruct.params.numMonomers; m1Idx++)
            {   
				
                fprintf(pFile,"%-6.6s %4.0d","CONECT",m1Idx+1);
                
				 
             for (int n1Idx=0; n1Idx<expStruct.rcl.connectedMonomers.rows(); n1Idx++)
             {
				 
                 if (expStruct.rcl.connectedMonomers(n1Idx,0)==(m1Idx+1))// the row including m1Idx
                 {
                     f = expStruct.rcl.connectedMonomers(n1Idx,1); // print the ind next to it
                     fprintf(pFile,"%4.0d ",f);
                 }                  
                 if (expStruct.rcl.connectedMonomers(n1Idx,1)==(m1Idx+1))// if it is in the second column
                 {
                     f = expStruct.rcl.connectedMonomers(n1Idx,0);// print the ind in the first column
                     fprintf(pFile,"%4.0d ",f);
                 }
             }
               fprintf(pFile,"\n");
            }
     
            fclose(pFile);   
            
  }
  return 1;  
}

//  Run relaxation simulation
experimentStruct PolymerRelaxation(experimentStruct &expStruct) {
    
    for(int sIdx = 0; sIdx < expStruct.params.numRelaxationSteps; sIdx++) 
        {
         expStruct.stepNum+=1;// increase counter
		 expStruct.rcl.Step();
		    if (expStruct.params.recordPositionDuringRelaxation)
	           {
				ExportCurrentPosition(expStruct);
               }
        }      
    return expStruct;
}

experimentStruct SimulatePreDamageStage(experimentStruct &expStruct){
    
   // record encounter frequency matrix   
   expStruct.encounterFreqMatPreUVC+=FindEncounters(expStruct);
  
   // before UVC, after relaxation         
    for(int sIdx = 0; sIdx < expStruct.params.numSteps; sIdx++) 
        {
         expStruct.stepNum+=1; // increse counter
		 expStruct.rcl.Step();
         expStruct.centerMass = expStruct.rcl.GetCenterOfMass();// record center of mass           
		  if (expStruct.params.recordPosition)
	           {
				   ExportCurrentPosition(expStruct);
				}
        }
    return expStruct;
}

experimentStruct SimulateDamageStage(experimentStruct &expStruct){
    // Simulate UVC laser affect on monomers, add exclusion volume around damaged monoemers
	 
	// get indices of monomers damaged 
    expStruct              = InduceUVC(expStruct);
    
    int numDamagedMonomers = expStruct.damagedMonomers.sum();  
  
    MatrixXf dmPosition;    // damage monomer position
        if (numDamagedMonomers==0){        
            dmPosition = MatrixXf::Constant(1,expStruct.rcl.params.numMonomers,0.0);
        }
        else {        
            dmPosition = MatrixXf::Constant(numDamagedMonomers,expStruct.params.dimension,0.0);
        }
      
     // get connected monomers after UVC 
     expStruct.connectedMonomers =  expStruct.rcl.connectedMonomers;
    int dmIdx = 0;
    
    for(int sIdx = 0; sIdx < expStruct.params.numDamageSteps; sIdx++)
        {
            
         expStruct.stepNum+=1;// increase counter 
         // step
         expStruct.rcl.Step();

          // update positions of damaged monomers
          dmIdx = 0;// running counter 
         for (int mIdx=0; mIdx<expStruct.rcl.params.numMonomers; mIdx++)
         {
            if (expStruct.damagedMonomers(mIdx)==1)
            {                
              dmPosition.row(dmIdx) = expStruct.rcl.curPos.row(mIdx);
              dmIdx+=1;
            }
         }
   
          if (numDamagedMonomers>0){
            // Apply outward pushing force centered at damaged monomers' positions
            
            expStruct.rcl.curPos = expStruct.force.HarmonicForce(expStruct.rcl.curPos,dmPosition,
                                        expStruct.params.harmonicExclussionForceMagnitude,
                                        expStruct.params.harmonicExclussionForceLowerCutoff,
                                        expStruct.params.harmonicExclussionForceUpperCutoff,
                                        expStruct.params.dt); 
          }
           expStruct.centerMass = expStruct.rcl.curPos.colwise().mean();// record center of mass 
           if (expStruct.params.recordPosition)
	           {
				   ExportCurrentPosition(expStruct);
				}
        }
                
    return expStruct;
}

experimentStruct SimulateRepairStage(experimentStruct &expStruct){
    // remove damaged monomers 
    expStruct.damagedMonomers = MatrixXf::Constant(expStruct.params.numMonomers,1,0);// repaired
    MatrixXf monomerDist      = MatrixXf::Constant(expStruct.params.numMonomers,expStruct.params.numExperiments,0.0);
    MatrixXf addedConnectors  = expStruct.rcl.connectedMonomers; //initial list 
    
    for (int sIdx = 0; sIdx<expStruct.params.numRepairSteps; sIdx++)
    {
        expStruct.stepNum+=1;
		expStruct.rcl.Step();

       // repair probability 
       // reform connections
       // allow monomers in close proximity to form connectors       
       // get monomer distance 
     
      if (addedConnectors.rows()<expStruct.rcl.params.numConnectors)// if less than initial value
      {
       monomerDist = utils.PairwiseDistance2(expStruct.rcl.curPos,expStruct.rcl.curPos);

       for (int pIdx =0; pIdx<expStruct.rcl.params.numMonomers;pIdx++)
       {
           for (int qIdx =pIdx; qIdx<expStruct.rcl.params.numMonomers; qIdx++)
           {
               if ((monomerDist(pIdx,qIdx)<expStruct.params.encounterDist) && (abs(pIdx-qIdx)>1))
               {
                expStruct.rcl.ConnectMonomers(pIdx,qIdx);
               }
           }
       }
   
       expStruct.rcl.GetConnectedMonomers("offDiagonals");
       addedConnectors = expStruct.rcl.connectedMonomers;
      }
      
       expStruct.centerMass = expStruct.rcl.GetCenterOfMass();// record center of mass     
       if (expStruct.params.recordPosition)
	      {
			ExportCurrentPosition(expStruct);
		  }
    }
   
    // record encounter frequency matrix 
    expStruct.encounterFreqMatPostUVC+=FindEncounters(expStruct);
     
return expStruct;    
}

// Run simulation and record Mean-square-displacement 
experimentStruct MSDSimulation(experimentStruct &expStruct){
    // Run polymer simulation
//    MatrixXf MSD          = MatrixXf::Constant(expStruct.rcl.params.numMonomers,expStruct.params.numMSDSteps, 0.0);
      MatrixXf rclEigenVecT = rcl.RouseEigenVectors(expStruct.params.numMonomers).transpose();

    if (expStruct.params.recordMSD==true){
        MatrixXf initPos = MatrixXf(expStruct.params.numMonomers,expStruct.params.dimension);
        if(expStruct.params.normalTransform == true) {
           initPos = rclEigenVecT*expStruct.rcl.curPos; // transform back to spatial coordinates
     } 
     else {
            initPos = expStruct.rcl.curPos;
         }
   
      MatrixXf msdPos = expStruct.rcl.curPos;      
      int N = expStruct.params.numMSDSteps;
      for(int sIdx = 0; sIdx < N; sIdx++) {                
        expStruct.rcl.Step();
        
      
        if(expStruct.params.normalTransform == true) {
            msdPos = rclEigenVecT *expStruct.rcl.curPos; // transform back to spatial coordinates
        } else {
            msdPos = expStruct.rcl.curPos; // keep spatial coordinates
        }                
		// for each monomers 		 iterative computation, accumulates over simulations 
        expStruct.msd.row(sIdx) = (expStruct.msd.row(sIdx)*(double(expStruct.simNum)-1.0) + 
		(msdPos- initPos).rowwise().squaredNorm().transpose())/double(expStruct.simNum); // in spatial coordinates
        
		if (expStruct.params.recordPositionDuringMSD){
			ExportCurrentPosition(expStruct);
		}
      }  
       // mean over all monomers in this simulation 
    expStruct.meanMSD.col(expStruct.simNum-1)= expStruct.msd.rowwise().mean();
    
    }
    return expStruct;
}

experimentStruct FitMSD(experimentStruct &expStruct){
    // take simulation results and fit alpha and beta to them 
    // the model to fit is beta*t^alpha, where alpha and beta are to be found, t is the time
    // in practice we compute the  estimators from the linear regression model y= log(beta)+alpha*log(t)
     if (expStruct.params.recordMSD==true){
       int N                        = expStruct.params.numMSDSteps;
      MatrixXf alpha        = MatrixXf::Constant(expStruct.rcl.params.numMonomers,1,0.0); // anomalouse exponent 
      MatrixXf beta          = MatrixXf::Constant(expStruct.rcl.params.numMonomers,1,0.0); // bias 
      MatrixXf timeVec   = MatrixXf::Constant(1,N,0.0);//log of the time
      MatrixXf timeVec2 = MatrixXf::Constant(1,N,0.0); // log squared of the time
       
     // mean MSD over all monomers (the center of mass)
      // omit the first point in the MSD computation of alpha and beta (to prevent zero in the ln term)
      MatrixXf msd                  = expStruct.msd;     // for all simulations 
      MatrixXf msdMonomer =MatrixXf::Constant(1,N-1,0.0); 
      MatrixXf lmsd                 =MatrixXf::Constant(1,N-1,0.0);// log of the msd
     
     // construct log-time vector and log time squared
      for (int sIdx=0; sIdx<N; sIdx++)
      {
          if (sIdx>0){// omit the first time point to prevent zero in log terms in alpha estimate below
                  timeVec(0,sIdx-1)   = log(double(sIdx)*expStruct.params.dt); // collect times in log
                  timeVec2(0,sIdx-1) = timeVec(0,sIdx-1) *timeVec(0,sIdx-1) ; // log squared
             } 
      }
      // compute the sum of the log time vectors
       double tSum = timeVec.sum();
       double tSum2 = timeVec2.sum();
       MatrixXf vTemp=MatrixXf::Constant(1,N-1,0.0);
       
       
       
      for (int mIdx=0; mIdx<expStruct.rcl.params.numMonomers; mIdx++){
          // compute log msd and multiplication
           for (int sIdx=0; sIdx<N-1;sIdx++){ 
              lmsd(0,sIdx)    = log(msd(sIdx+1,mIdx)); 
              vTemp(0,sIdx) = lmsd(0,sIdx)*timeVec(0,sIdx);
           }
          alpha(mIdx,0) = (N*(vTemp).sum() -(tSum)*(lmsd.sum()) )/((N)*tSum2 -tSum*tSum);     
          beta(mIdx,0)   = exp((lmsd.sum() -alpha(mIdx,0)*tSum)/(N));
      }
   cout<<"alpha"<<endl;
      expStruct.msdAlpha.col(expStruct.expNum-1) = alpha;
      expStruct.msdBeta.col(expStruct.expNum-1) = beta;
     }
      
    return expStruct;
}

experimentStruct FETSimulation(experimentStruct &expStruct) {
    // preallocations 
    float encounterTime   = 0.0;
    if (expStruct.params.transientSimulation)
    {      
        bool runFlag           = true;
        bool eIndicator        = false;
        int  stepIdx                 = 1.0; // step counter
        int  numLpoints        = 150; // number of points along the trajectory from prev to pos for which we check monomer encounter
        MatrixXf rouseEigenVec = rcl.RouseEigenVectors(expStruct.params.numMonomers);
        MatrixXf posT          = MatrixXf::Constant(expStruct.params.numMonomers, expStruct.params.dimension, 0.0);
        MatrixXf prevPosT      = MatrixXf::Constant(expStruct.params.numMonomers, expStruct.params.dimension, 0.0);    
        MatrixXf l1            = MatrixXf::Constant(numLpoints,expStruct.params.dimension,0.0);
        MatrixXf l2            = MatrixXf::Constant(numLpoints,expStruct.params.dimension,0.0);    
        MatrixXf lDistances    = MatrixXf::Constant(numLpoints,1,0.0);
    
    while(runFlag == true) {
        // record previous polymer position
        if (expStruct.params.normalTransform==true)
        {
            prevPosT = (rouseEigenVec.transpose() * expStruct.rcl.curPos);

        } else if(expStruct.params.normalTransform == false) {
            prevPosT = expStruct.rcl.curPos;
        }
		
        expStruct.stepNum+=1;// increase counter
        // Simulation step 
		 expStruct.rcl.Step();
               
        // Transform to spatial  coordinates if needed
        if(expStruct.params.normalTransform == true) {
            posT = (rouseEigenVec.transpose() * expStruct.rcl.curPos);// transform back to spatial coordinates

        } else if(expStruct.params.normalTransform == false) {
            posT = expStruct.rcl.curPos; // keep spatial coordinates
        }
                 
         if (expStruct.params.recordPositionDuringFET)
		 {
			 ExportCurrentPosition(expStruct);
		 }
        // Colision detection
        // set equal intervals along the path between prevPosT and posT
         for (int lIdx=0; lIdx<numLpoints;lIdx++)  
         {
           for (int dIdx=0; dIdx<expStruct.params.dimension; dIdx++)  
           {
            l1(lIdx,dIdx) = prevPosT(expStruct.params.fetMonomer1-1,dIdx)+(float(lIdx)/numLpoints) *(posT(expStruct.params.fetMonomer1-1,dIdx)- prevPosT(expStruct.params.fetMonomer1-1,dIdx));
            l2(lIdx,dIdx) = prevPosT(expStruct.params.fetMonomer2-1,dIdx)+(float(lIdx)/numLpoints) *(posT(expStruct.params.fetMonomer2-1,dIdx)- prevPosT(expStruct.params.fetMonomer2-1,dIdx));
           }
         }
         
        // Compute distances at successive points 
        lDistances   = (l1-l2).rowwise().norm();
        int firstInd = numLpoints;

        for (int lIdx=0; lIdx<lDistances.rows(); lIdx++)
        {
            if (lDistances(lIdx,0)<(expStruct.params.encounterDist))
            {
              eIndicator = true;
              if (lIdx<firstInd)
              {
                firstInd   = lIdx;              
              }
            }
        }
        // the encounter time on a fine scale of dt
        encounterTime = float(stepIdx-1)*expStruct.params.dt +float(firstInd)*expStruct.params.dt/float(numLpoints);
        stepIdx += 1; // increase step counter by 1s

        // check exit conditions        
        if (eIndicator==true){ 
            runFlag = false;
            cout << "Encouter time= " << encounterTime << "[s]"<<endl;
        }

        if(stepIdx > expStruct.params.maxFETsteps) {
            runFlag = false;
            cout << " Max. FET steps reached. Continuing to the next simulation" << endl;
        }
      }
    } 
    expStruct.fet(0,expStruct.simNum-1) = encounterTime;
    //average
    expStruct.mfet(0,expStruct.expNum-1)    = expStruct.fet.mean();

    return  expStruct;
}

experimentStruct ComputeTwoSidedEncounterProbability(experimentStruct &expStruct){
    // Compute the 2 sided encounter probability from the encounter frequency matrix 
    MatrixXf sR = expStruct.encounterFreqMat.rowwise().sum();   
    
//    MatrixXf encounterProb2Sides =MatrixXf::Constant(results.encounterFreqMat.rows(), results.encounterFreqMat.rows(),0.0);
    for (int mIdx=0; mIdx<expStruct.encounterFreqMat.rows(); mIdx++)
    {
        
        for (int nIdx=0; nIdx<expStruct.rcl.params.numMonomers; nIdx++)
        {
         expStruct.encounterProb2Sides(mIdx,nIdx) = float(expStruct.encounterFreqMat(mIdx,nIdx))/float(sR(mIdx));
        }
    }    
    return expStruct;
}

MatrixXf ComputeVariance(MatrixXf posX, MatrixXf posY,MatrixXf posZ){
      cout<<"computing variance"<<endl;
      
        MatrixXf  Vx      = MatrixXf::Constant(1,posX.cols(),0.0);              
        MatrixXf  Vy      = MatrixXf::Constant(1,posY.cols(),0.0);
        MatrixXf  Vz      = MatrixXf::Constant(1,posZ.cols(),0.0);
        MatrixXf variance = MatrixXf::Constant(posX.rows(), posX.rows(),0.0);
        
        int numMonomers    = posX.rows();
        int numSimulations = posX.cols();
        for (int mIdx=0; mIdx<numMonomers; mIdx++)
        {            
            for (int nIdx=0; nIdx<numMonomers; nIdx++)
            {   
                                
                for (int pIdx=0; pIdx<numSimulations;pIdx++)
                {
                  Vx(0,pIdx)= posX(mIdx,pIdx)- posX(nIdx,pIdx);
                  Vy(0,pIdx)= posY(mIdx,pIdx)- posY(nIdx,pIdx);
                  Vz(0,pIdx)= posZ(mIdx,pIdx)- posZ(nIdx,pIdx);
                }

                 variance(mIdx,nIdx) = (Vx.colwise()-Vx.rowwise().mean()).squaredNorm()/float(numSimulations)+
                                       (Vy.colwise()-Vy.rowwise().mean()).squaredNorm()/float(numSimulations)+
                                       (Vz.colwise()-Vz.rowwise().mean()).squaredNorm()/float(numSimulations);                                        
            }
        }
        
        return variance;
}

experimentStruct ComputeOneSideEncounterProbability(experimentStruct &expStruct){
    // Compute the 1 sided encounter probability from the encounter frequency matrix 
    float sR = 0.0;
    for (int mIdx=0; mIdx<expStruct.encounterFreqMat.rows(); mIdx++)
    {
        for (int nIdx=0; nIdx<expStruct.encounterFreqMat.rows(); nIdx++)
        {
          sR +=expStruct.encounterFreqMat(mIdx,nIdx);  
        }
        
        expStruct.encounterProb1Side.block(mIdx,0,0,expStruct.encounterProbMat.rows()-1) = 
        expStruct.encounterFreqMat.block(mIdx,0,0,expStruct.encounterProbMat.rows()-1)/sR;
        
    }
    return expStruct;
}

int ExportFETData(experimentStruct &expStruct){
        std::stringstream fetFileName; 
        fetFileName <<"FirstEncounterTime_Exp_"<<expStruct.expNum<<".csv"; // file name 
        string fetFilePath=utils.CombinePaths(expStruct.paths.fet, fetFileName.str()); // file path 
        std::ofstream fetResultFile(fetFilePath,std::ofstream::ate);
        
        // Write srg data
        for (int sIdx=0; sIdx<expStruct.params.numSimulations; sIdx++)
        {
          fetResultFile<<expStruct.fet(0,sIdx)<<",";
          fetResultFile<<endl;
        }
        
        fetResultFile.close();
    
    return 0;

}

int ExportSRGData(experimentStruct &expStruct){
    // export the square radius of gyration to a csv file 
        std::stringstream srgFileName; 
        srgFileName <<"SquareRadiusOfGyration_Exp_"<<expStruct.expNum<<".csv"; // file name 
        string srgFilePath=utils.CombinePaths(expStruct.paths.srg, srgFileName.str()); // file path 
        std::ofstream srgResultFile(srgFilePath,std::ofstream::app);
        
        // Write srg data
        for (int sIdx=0; sIdx<expStruct.params.numSimulations; sIdx++)
        {
          srgResultFile<<expStruct.srg(0,sIdx)<<",";
          srgResultFile<<endl;
        }
        
        srgResultFile.close();
    
    return 0;
}

int ExportMSDData(experimentStruct &expStruct){
    
		//export MSD for each monomer
      if (expStruct.params.recordMSD==true){
       std::ofstream msdResultFilemonomers(expStruct.fileNames.MSD,std::ofstream::app);

        // Write  MSD data for each monomer in each step 
		for (int sIdx=0; sIdx<expStruct.params.numMSDSteps; sIdx++)   
            {
         for (int mIdx=0; mIdx<expStruct.rcl.params.numMonomers; mIdx++)
          {           
                msdResultFilemonomers<<expStruct.msd(sIdx,mIdx)<<",";
            }
           msdResultFilemonomers<<endl;
          }
        msdResultFilemonomers.close();
    //  export mean msd over all monomers 
    std::ofstream msdResultFile(expStruct.fileNames.meanMSD,std::ofstream::app);

    MatrixXf meanMSD=MatrixXf::Constant(expStruct.params.numMSDSteps,1,0.0);
    for (int sIdx=0; sIdx<expStruct.params.numMSDSteps;sIdx++){
         for (int eIdx=0; eIdx<expStruct.params.numSimulations; eIdx++){
           msdResultFile<<expStruct.meanMSD(sIdx,eIdx)<<",";     
         }
        msdResultFile<<endl;
     }
     msdResultFile.close();
      }
      
    return 0;
}

int ExportEncounterFrequency(experimentStruct &expStruct){
    // write out encounters
	
        std::ofstream encounterFreqFile(expStruct.fileNames.encounterFrequency,std::ofstream::app);// (append)
        for (int m1Idx=0; m1Idx<expStruct.rcl.params.numMonomers; m1Idx++)
        {
            for (int m2Idx=0; m2Idx<expStruct.rcl.params.numMonomers; m2Idx++)
            {
              encounterFreqFile << expStruct.encounterFreqMat(m1Idx,m2Idx)<<",";
            }
            encounterFreqFile<< endl;
        }
          encounterFreqFile.close();
    
    return 0;
}

int ExportEncounterProb2Sides(experimentStruct &expStruct){
	// TODO move function to ResultHandler
         std::ofstream monomerEncountersProb2SidesFile(expStruct.fileNames.encounterProb2Sides,std::ofstream::app);// (append)        
        for (int m1Idx=0; m1Idx<expStruct.rcl.params.numMonomers; m1Idx++)
        {
            for (int m2Idx=0; m2Idx<expStruct.rcl.params.numMonomers; m2Idx++)
            {               
              monomerEncountersProb2SidesFile << expStruct.encounterProb2Sides(m1Idx,m2Idx)<<",";
            }
            monomerEncountersProb2SidesFile<< endl;
        }
          monomerEncountersProb2SidesFile.close();
          
    return 0;
}

int ExportVariance(experimentStruct &expStruct){
            // write out variance            
         std::ofstream  varianceFilePointer(expStruct.fileNames.variance,std::ofstream::app);
            
            for (int m1Idx=0; m1Idx<expStruct.rcl.params.numMonomers; m1Idx++)
            {
                for (int m2Idx=0; m2Idx<expStruct.rcl.params.numMonomers; m2Idx++)
                {
                  varianceFilePointer << expStruct.varianceSim(m1Idx,m2Idx)<<",";
                }
            varianceFilePointer<< endl;
            }
            varianceFilePointer.close();
    
    
    return 0;
}

int ExportSimulationResults(experimentStruct& expStruct){
	// export results of the current simulation
	   // Export last polymer configuration
		bool ep = expStruct.params.recordPosition;
        int ri       = expStruct.params.recordPositionInterval;
		expStruct.params.recordPosition=true;
        expStruct.params.recordPositionInterval = 1;
		ExportCurrentPosition(expStruct);
		ExportConnectedMonomersList(expStruct);
		expStruct.params.recordPosition=ep;// return to original value
        expStruct.params.recordPositionInterval = ri;

	return 0;
}

int ExportExperimentResults(experimentStruct& expStruct){
		// export all results gathered in the simulations of the current experiment
        float tempXi = expStruct.rcl.params.connectivity;
        
        if(expStruct.rcl.params.normalTransform == false) {
            
            tempXi = 2.0*expStruct.rcl.params.connectivity * float(expStruct.rcl.params.numConnectors)/(float(expStruct.rcl.params.numMonomers) + float(expStruct.rcl.params.numConnectors));
        } else {
            tempXi = expStruct.rcl.params.connectivity;
        }
        
        // Compute theoretical values of MSRG and MFET
        float mfetTheory = rcl.MeanFirstEncounterTime(expStruct.params.fetMonomer1, expStruct.params.fetMonomer2, tempXi, expStruct.rcl.params.numMonomers,
                                                      expStruct.params.encounterDist, expStruct.rcl.params.D, expStruct.rcl.params.b,expStruct.rcl.params.dimension); 
        float msrgTheory = rcl.MeanSquareRadiusOfGyration(expStruct.rcl.params.numMonomers, tempXi, expStruct.rcl.params.b, expStruct.rcl.params.dimension);

       // Write results to csv at the end of simulation cycle         
        std::ofstream resultSummary(expStruct.paths.resultSummary, std::ofstream::app); // Opening file to print info to (append)        
		// adds a line
        resultSummary << expStruct.expNum     << "," << expStruct.msrg(0,expStruct.expNum-1)<< "," << msrgTheory << "," << expStruct.mfet(0,expStruct.expNum-1)
                      << "," << mfetTheory    << "," << expStruct.params.fetMonomer1       << ","<< expStruct.params.fetMonomer2 << "," << 0.0 
                      << "," << tempXi        << "," << expStruct.rcl.params.numConnectors << ","<< expStruct.rcl.params.b       << "," << expStruct.rcl.params.D 
                      << "," << expStruct.rcl.params.numMonomers<< "," << expStruct.params.dt            << ","<< expStruct.params.dtRelaxation << "," << expStruct.rcl.params.normalTransform
                      << "," << expStruct.params.numSimulations << "," << expStruct.params.numSteps      << ","<< expStruct.params.recordMSD << "," << expStruct.params.numMSDSteps 
                      << "," << expStruct.params.minMonomerDist << "," << expStruct.params.encounterDist << endl;
        resultSummary.close();
       
        // Report simulation vs theory 
        cout << "-------- Summary -------"<<endl;
        cout << "MSRG simulations= " << expStruct.msrg(0,expStruct.expNum-1)<< endl;
        cout << "MSRG theory     = " << msrgTheory << endl;

        if(expStruct.params.transientSimulation == true) {
            cout << "MFET simulation = " << expStruct.mfet(0,expStruct.expNum-1) << " [sec]" << endl;
            cout << "MFET theory     = " << mfetTheory<< " [sec]"<<endl;
        }
        // Export results to csv files
        ExportEncounterFrequency(expStruct);
        
        ExportEncounterProb2Sides(expStruct);

        ExportVariance(expStruct);
        
        // Export square radius of gyration data
        ExportSRGData(expStruct);
        
        // export FET data
        ExportFETData(expStruct);
        
        ExportMSDData(expStruct);// export MSD for each simulation 
        return 0;
}

int ExportMSDFitParams(experimentStruct &expStruct){
    
         //export alpha    
        string msdFitAlphaFileName=utils.CombinePaths(expStruct.paths.msdFit , "alpha.csv");
        std::ofstream msdFitAlphaFile(msdFitAlphaFileName,std::ofstream::app);
        // prepare headers
        for (int eIdx=0; eIdx<expStruct.params.numExperiments; eIdx++)
        {        
           msdFitAlphaFile<<"experiment"<<eIdx+1<<",";
        }
        msdFitAlphaFile<<endl;
        // Write srg data
        for (int sIdx=0; sIdx<expStruct.params.numSimulations; sIdx++)
        {
            for (int eIdx=0; eIdx<expStruct.params.numExperiments; eIdx++)
            {
                msdFitAlphaFile<<expStruct.msdAlpha(sIdx,eIdx)<<",";
            }
          msdFitAlphaFile<<endl;
        }
        
        msdFitAlphaFile.close();
        
        
    // export beta
            
        string msdFitBetaFileName=utils.CombinePaths(expStruct.paths.msdFit , "beta.csv");
        std::ofstream msdFitBetaFile(msdFitBetaFileName,std::ofstream::app);
        // prepare headers
        for (int eIdx=0; eIdx<expStruct.params.numExperiments; eIdx++)
        {        
           msdFitBetaFile<<"experiment"<<eIdx+1<<",";
        }
        msdFitBetaFile<<endl;
        // Write srg data
        for (int sIdx=0; sIdx<expStruct.params.numSimulations; sIdx++)
        {
            for (int eIdx=0; eIdx<expStruct.params.numExperiments; eIdx++)
            {
                msdFitBetaFile<<expStruct.msdBeta(sIdx,eIdx)<<",";
            }
          msdFitBetaFile<<endl;
        }
        
        msdFitBetaFile.close();

    return 0;
}

paramStruct ParseInputParams(paramStruct params,int argc, char** argv){
    // parse input parameters
    if (argc>0){
        int numArg = argc/2;
         //TODO add parser check that it is name value pairs
        for (int argIdx=1; argIdx<(numArg+1); argIdx++)
        {// assuming it is name value pairs
        
         std::string stName(argv[2*argIdx-1]);
         std::string stVal(argv[2*argIdx]);
        if (stName=="-numSimulations"){
            cout<<" numSimulations in ="<< stVal<<endl;
            params.numSimulations = stoi(stVal);
        } else if (stName== "-numExperiments"){
            cout<<"numExperiments in "<<stVal<<endl;
            params.numExperiments = stoi(stVal);
        } else if (stName =="-numRelaxationSteps"){
            cout<<"numRelaxationSteps in "<<stVal<<endl;
            params.numRelaxationSteps = stoi(stVal);            
         } else if (stName =="-numSteps"){
             cout<<"numSteps in "<<stVal<<endl;
             params.numSteps = stoi(stVal);
         }else if (stName =="-numDamageSteps"){
             cout<<"numDamageSteps in "<<stVal<<endl;
             params.numDamageSteps = stoi(stVal);
         }else if (stName =="-numRepairSteps"){
             cout<<"numRepairSteps in "<<stVal<<endl;
             params.numRepairSteps = stoi(stVal);
         }else if (stName =="-numMonomers"){
             cout<<"numMonomers in "<<stVal<<endl;
             params.numMonomers = stoi(stVal);
         } else if (stName =="-numConnectors"){
             cout<<"numConnectors in "<<stVal<<endl;
             params.numConnectors = stoi(stVal);
         } else if (stName =="-dimension"){
             cout<<"dimension in "<<stVal<<endl;
             params.dimension = stoi(stVal);
         } else if (stName == "-maxFETsteps"){
             cout<<"maxFETsteps in "<<stVal<<endl;
             params.maxFETsteps = stoi(stVal);
         } else if (stName == "-numMSDSteps"){
             cout<<"numMSDSteps in "<<stVal<<endl;
             params.numMSDSteps = stoi(stVal);
         } else if (stName == "-fetMonomer1"){
             cout<<"fetMonomer 1 in "<<stVal<<endl;
             params.fetMonomer1 = stoi(stVal);
         } else if (stName == "-fetMonomer2"){
             cout<<"fet monomer 2 in "<<stVal<<endl;
             params.fetMonomer2 = stoi(stVal);
         }else if (stName == "-recordPositionInterval"){
             cout<<"recordPositionInterval in "<<stVal<<endl;
             params.recordPositionInterval = stoi(stVal);
         }else if (stName =="-D"){
             cout<<"diffusion const in "<<stVal<<endl;
             params.D = std::stof (stVal);
         }else if (stName =="-b"){
             cout<<"conector std in "<<stVal<<endl;
             params.b = std::stof (stVal);
         }else if (stName =="-dt"){
             cout<<"dt in "<<stVal<<endl;
             params.dt = std::stof (stVal);
         }else if (stName =="-dtRelaxation"){
             cout<<"dtRelaxation in "<<stVal<<endl;
             params.dtRelaxation = std::stof (stVal);
         }else if (stName =="-encounterDist"){
             cout<<"encounter dist in "<<stVal<<endl;                          
             params.encounterDist = std::stof (stVal);
         }else if (stName =="-UVClaserRadius"){
             cout<<"UVClaserRadius in "<<stVal<<endl;                          
             params.UVClaserRadius = std::stof (stVal);
         }else if (stName =="-harmonicExclussionForceMagnitude"){
             cout<<"harmonicExclussionForceMagnitude in "<<stVal<<endl;                     
             params.harmonicExclussionForceMagnitude = std::stof (stVal);
         }else if (stName =="-harmonicExclussionForceLowerCutoff"){
             cout<<"harmonicExclussionForceLowerCutoff in "<<stVal<<endl;            
             params.harmonicExclussionForceLowerCutoff = std::stof (stVal);
         }else if (stName =="-harmonicExclussionForceUpperCutoff"){
             cout<<"harmonicExclussionForceUpperCutoff in "<<stVal<<endl;            
             params.harmonicExclussionForceUpperCutoff = std::stof (stVal);
         }else if (stName =="-normalTransform"){
             cout<<"normalTransform in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.normalTransform = true;
                   }else if (stVal=="false"){
                       params.normalTransform = false;
                   }
         }else if (stName =="-transientSimulation"){
             cout<<"transientSimulation in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.transientSimulation = true;
                   }else if (stVal=="false"){
                       params.transientSimulation = false;
                   }
         }else if (stName =="-recordMSD"){
             cout<<"recordMSD in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.recordMSD = true;
                   }else if (stVal=="false"){
                       params.recordMSD = false;
                   }
         }else if (stName =="-recordPosition"){
             cout<<"recordPosition in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.recordPosition = true;
                   }else if (stVal=="false"){
                       params.recordPosition = false;
                   }
         }else if (stName =="-recordPositionDuringRelaxation"){
             cout<<"recordPositionDuringRelaxation in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.recordPositionDuringRelaxation = true;
                   }else if (stVal=="false"){
                       params.recordPositionDuringRelaxation = false;
                   }
         }else if (stName =="-recordPositionDuringFET"){
             cout<<"recordPositionDuringFET in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.recordPositionDuringFET = true;
                   }else if (stVal=="false"){
                       params.recordPositionDuringFET = false;
                   }
         }else if (stName =="-recordPositionDuringMSD"){
             cout<<"recordPositionDuringMSD in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.recordPositionDuringMSD = true;
                   }else if (stVal=="false"){
                       params.recordPositionDuringMSD = false;
                   }
         }else if (stName =="-harmonicExclussionForce"){
             cout<<"harmonicExclussionForce in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.harmonicExclussionForce = true;
                   }else if (stVal=="false"){
                       params.harmonicExclussionForce = false;
                   }
         }else if (stName =="-breakConnectorsFromDamagedMonomers"){
             cout<<"breakConnectorsFromDamagedMonomers in "<<stVal<<endl;
               if (stVal=="true"){                                        
                   params.breakConnectorsFromDamagedMonomers = true;
                   }else if (stVal=="false"){
                       params.breakConnectorsFromDamagedMonomers = false;
                   }
         }else if (stName =="-resultBaseFolder"){
             cout<<"resultBaseFolder in "<<stVal<<endl;                     
             params.resultBaseFolder = stVal;
        }
     } 
    }
    return params;
}

int main(int argc, char** argv)
{
    // Initialize parameter structure with default values
    paramStruct params; 
    // parse input parameters
    params = ParseInputParams(params,argc, argv);        
    // number of connectors in each experiment     
//    MatrixXf numConnectors=MatrixXf::Constant(1,50,0);
    // BUG the results are not saved if the result directory does not exists
	utils.createDir(params.resultBaseFolder);

	// Initialize experiment structure with params
    experimentStruct expStruct;
    expStruct.Initialize(params);
    
    // Start experiments loop
    for(int expIdx = 0; expIdx < expStruct.params.numExperiments; expIdx++) {
        // Update expStruct and associated data structures
        expStruct.NewExperiment();

        // Rouse eigenvectors 
        MatrixXf rouseEigenVec   = expStruct.rcl.RouseEigenVectors(expStruct.rcl.params.numMonomers);
        MatrixXf rouseEigenVecT = rouseEigenVec.transpose();

        // Start simulation cycle
        for(int sIdx = 0; sIdx < expStruct.params.numSimulations; sIdx++) {
            
            // Update expStruct
            expStruct.NewSimulation();
            cout<<"==> Experiment ("<<expStruct.expNum<<"/"<<params.numExperiments<<")"<<endl;
            cout<<"    Simulation ("<<expStruct.simNum<<"/"<<params.numSimulations<<")"<<endl;
            
            clock_t runTime   = utils.Tic();
            
            cout<<"Simulation polymer to relaxation....";
            PolymerRelaxation(expStruct);
            cout<<"done."<<endl;

            SimulatePreDamageStage(expStruct);
            
            SimulateDamageStage(expStruct);

            SimulateRepairStage(expStruct);
            
            expStruct.RecordLastPolymerPosition();
            
            // Record encounters
            expStruct.encounterFreqMat +=FindEncounters(expStruct);

            // Record square radius of gyration
            expStruct.srg(0,sIdx) = expStruct.rcl.ComputeSquareRadiusOfGyration();

            // Perform MSD simulation 
            cout<<"Recording MSD.... ";
            MSDSimulation(expStruct);
            cout<<"done."<<endl;

            // Perform Transient (FET) simulation 
            cout<<"Simulation FET...."<<endl;
            FETSimulation(expStruct);
            cout<<"done."<<endl;

            // export results 
            cout<<"Exporting simulation results...";
            ExportSimulationResults(expStruct);
            cout<<"done."<<endl;
            
            cout<<"Simulation "<<expStruct.simNum<<" Done."<<endl;
            utils.Toc(runTime);
            cout<<endl;
        } // end simulation cycle

         //---------- Post simulation cycle actions ---------------------
         cout<<"Fit MSD....";
         FitMSD(expStruct);// fit msd to all simulation, all monomers and theie average MSD.
         cout<<"done"<<endl;
               
        // Compute variance from the end position of each simulation         
        cout<<"compute variance....";
        expStruct.varianceSim       = ComputeVariance(expStruct.posX,expStruct.posY,expStruct.posZ);
        cout<<"done"<<endl;
        
        cout<<"compute msrg....";
        expStruct.msrg(0,expIdx)   = expStruct.srg.mean();
        cout<<"done"<<endl;
        
        // Compute encounter probability 2 sided
        cout<<"computing encounter probability"<<endl;
        ComputeTwoSidedEncounterProbability(expStruct);
        cout<<"done"<<endl;
        
        // Compute mean first encounter time 
//        expStruct.mfet(0,expIdx) = expStruct.fet.mean();
       
        // Export Simulation results
cout<<"export experiment results"<<endl; 
        ExportExperimentResults(expStruct);
        cout<<"done"<<endl;
    }// end experiment cycle 
   
//     ExportMSDData(expStruct);
      // Export MSDFit
     ExportMSDFitParams(expStruct);
   
    return 0;
};