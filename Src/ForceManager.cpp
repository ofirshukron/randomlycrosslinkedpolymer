using namespace std;
using namespace Eigen;

class ForceManager
{
 public:
 

MatrixXf HarmonicForce(MatrixXf pos, MatrixXf sourcePosition, float forceMagnitude, float lowerCutOff, float upperCutOff, float dt)                               
{    
    // Apply harmonic spring force on all monomers (pull or push force) 
    
        MatrixXf distToSource = utils.PairwiseDistance1(pos,sourcePosition);
        MatrixXf L            = MatrixXf::Constant(1,sourcePosition.rows()+1,0.0);// indicator matrix
        MatrixXf p            = MatrixXf::Constant(sourcePosition.rows()+1,pos.cols(),0.0);
       
        for (int pIdx=1;pIdx<p.rows();pIdx++)
        {
            p.row(pIdx) = sourcePosition.row(pIdx-1);        
        }
        
        for (int mIdx = 0; mIdx<pos.rows(); mIdx++)// for each monomer
        { 
            
           L = MatrixXf::Constant(1,sourcePosition.rows()+1,0.0);
         
          for (int sIdx=0; sIdx<sourcePosition.rows(); sIdx++)// for each source
          {
              if ((distToSource(mIdx,sIdx)>lowerCutOff) && (distToSource(mIdx,sIdx)<upperCutOff))
              {
                  L(0,sIdx+1)=1;// indicate that the force from source sIdx is applied 
              }
          }
          
          L(0,0)= -L.sum();
         
          p.row(0)= pos.row(mIdx);
          pos.row(mIdx) += (forceMagnitude*dt)*L*p;
        }
   
   return pos;          
}


};