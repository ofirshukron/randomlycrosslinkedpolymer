//# include <iostream>
//# include <stdio.h>
//# include <math.h>

using namespace std;
using namespace Eigen;
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<> nDist(0.0,1.0); // rand normal generator
# define PI 3.14159265358979323846

/**
 * @class RandomlyCrossLinkedPolymer
 * @author ofir
 * @date 02/13/17
 * @file RandomlyCrossLinkedPolymer.cpp
 * @brief 
 */
 
class RandomlyCrossLinkedPolymer
{
public:    

struct RCLparams{
	int    numMonomers;
	int    numConnectors;
	double connectivity;
	int    dimension;
	float  D;
	float  b;
	float  dt;
	bool   normalTransform;
	MatrixXf fixedConnectors; // monomer pairs connected 
};

//*TODO add ForceManager, move forces there

//MatrixXf position;
int      stepNum=0;; // step number 
MatrixXf curPos;
MatrixXf prevPos;
MatrixXf laplacian;
MatrixXf connectedMonomers;
MatrixXf connectivityMat;  // binar matrix of connectivity
MatrixXf centerOfMass; // polymer's center of mass 
float srg = 0.0; //square radius of gyration 
int numConnectedMonomers=0; // number of monomer pairs connected

RCLparams params;

int Initialize(int numMonomers, int numConnectors,MatrixXf fixedConnectors,int dimension,float D,float connectorSTD,float dt,bool normalTransform)
{
	/* set parameters*/
	params.numMonomers     = numMonomers;
	params.numConnectors   = numConnectors;
	params.connectivity    = 2.0*params.numConnectors/((double(numMonomers)-1.0)*(double(numMonomers)-2.0));
	params.dt              = dt;
	params.b               = connectorSTD;
	params.normalTransform = normalTransform;
	params.dimension       = dimension;
	params.D               = D;
	params.fixedConnectors = fixedConnectors; 
	numConnectedMonomers   = params.numConnectors+params.numMonomers;//initial values of connected pairs
	stepNum                = 0;
	/* Initialize the polymer */
	InitializePolymerPosition();
	GetCenterOfMass();
	GraphLaplacian(params.numMonomers,params.numConnectors,params.normalTransform);
    GetConnectedMonomers("offDiagonals");// populate connected mononer list
	
	return 0;
}

int InitializePolymerPosition()
{

	prevPos = MatrixXf::Constant(params.numMonomers,params.dimension,0.0);
    // Initialize chain position     
    for (int mIdx=1; mIdx<params.numMonomers;mIdx++)
    {
	  prevPos.row(mIdx)= prevPos.row(mIdx-1)+Noise(1,params.dimension,params.D,params.dt);     
    }
    
    // normal transform
    if (params.normalTransform)
    {
        prevPos  = prevPos*RouseEigenVectors(params.numMonomers);
    }
     curPos = prevPos;
    return 1; 
}

/*!
 * @brief Compute the mean square radius of gyration 
 * @details 
 * @param numMonomers the number of monomers
 * @param connectivityFraction
 * @param connectorSTD
 * @param dimension
 * @return 
 */
float MeanSquareRadiusOfGyration(int numMonomers, float connectivityFraction, float connectorSTD,int dimension)
{
    float y  = 1.0+float(numMonomers)*connectivityFraction/(2.0*(1.0-connectivityFraction));
    float z0 = y+sqrt(pow(y,2.0)-1.0);
    float z1 = y-sqrt(pow(y,2.0)-1.0);
    float x  = connectivityFraction; 
    float b  = connectorSTD;
    float N  = float(numMonomers);
    float msrg = (pow(b,2.0)/((1.0-x)*(z0-z1)*pow(N,2.0)))
                *(pow(z0,-2*N)*(-z0*(1.0 + N*(-1.0 + z0) + z0)
                 +pow(z0,2*N)*(z0 + (-1.0 + N*(-1.0 + z0))*
                 (-N + (-1.0 + N)*pow(z0,2)))))/(pow(-1.0 + z0,2.0)*(1.0 + z0));
				
    return msrg;
}

MatrixXf MeanSquareDisplacement(int numMonomers, int dimension, float dt, int numSteps, float diffusionConst, float connectorSTD, float connectivityFraction)
{
        VectorXi t   = VectorXi::LinSpaced(numSteps,0.0, (float(numSteps)-1.0)*dt); // time        
        MatrixXf msd = MatrixXf::Constant(1,numSteps,0.0);
        float N     = float(numMonomers);
        float d     = float(dimension);
        float b     = connectorSTD;
        float xi    = connectivityFraction;
        float D     = diffusionConst;
        
        if (xi>0)
        {
            for (int tIdx; tIdx<numSteps; tIdx++) {
                msd(tIdx)   = 2.0*d*(diffusionConst/N)*t(tIdx)+
                              sqrt(d*(pow(b,2.0)))/
                                   (2.0*sqrt(N*xi*(1.0-xi)))*
                                   erf(sqrt(2.0*D*N*xi*t(tIdx)/pow(b,2.0)));
            }
        } else if (xi==0)
        {
            for (int tIdx; tIdx<numSteps; tIdx++) {
                msd(tIdx)   = 2*(D/N)*t(tIdx)+b*sqrt(D*t(tIdx)/PI);// Rouse polymer
            }
        }
        
        return msd;
}

float EncounterProbability(int monomer1, int monomer2, int numMonomers, float connectivityFraction, float connectorSTD, int dimension)
{
    // Encounter probabilty between monomer 1 and 2 for a polymer with numMonomers monomers and connectivityFraction
    float ep = 0.0;
    ep = pow(float(dimension)/(2*PI*Variance(monomer1, monomer2,connectivityFraction,numMonomers,connectorSTD)),float(dimension)/2.0);
    return ep;
}

/**
 *  Compute the variance betwenn two monomers of the RCL polymer
 * @param[in] monomer1 
 * @param[in] monomer2
 * @param[in] xi
 * @param N number of monomers
 * @param b std of connectors length
 * @return variance between monomers
 **/
float Variance(int monomer1, int monomer2, float xi, int N, float b)
{        
            
            float y     = 1.0+float(N)*xi/(2.0*(1.0-xi));
            float z0    = y+sqrt(pow(y,2.0) -1.0);
            float z1    = y-sqrt(pow(y,2.0) -1.0);
            float m     = float(monomer1);
            float n     = float(monomer2);
            float sigma = 0.0;
            
                if (n>m)
                {                 
                       sigma = ((1.0/pow(z0,2.0*n-1.0))*(pow(pow(z0,(n-m))-1.0,2.0) -2.0*pow(z0,(m+n-1.0)))+2+
                                (1.0/pow(z0,(2.0*(N-m+1.0)-1.0)))*(pow(pow(z0,N-m+1-(N-n+1.0))-1.0,2.0) -2.0*pow(z0,(N-m+1.0+N-n+1.0-1.0)))+2.0)/2.0;
                }
                 else {                    
                       sigma = ((1.0/pow(z0,2.0*m-1.0))*(pow(pow(z0,m-n)-1.0,2.0) -2.0*pow(z0,m+n-1.0))+2.0+
                                (1.0/pow(z0,2.0*(N-n+1.0)-1.0))*(pow((pow(z0,N-n+1.0-(N-m+1.0))-1.0),2.0)-2.0* pow(z0,(N-m+1.0+N-n+1.0-1.0)))+2.0)/2.0;
                 }                
            
            sigma = sigma*(pow(b,2.0)/((1.0-xi)*(z0-z1)));
        return sigma;   
};

float MeanFirstEncounterTime(int monomer1, int monomer2, float connectivityFraction, int numMonomers,float encounterDist, float diffusionConst, float b, int dimension)
{
    
    //cout<<monomer1<<","<<monomer2<<","<<connectivityFraction<<","<<numMonomers<<","<<encounterDist<<","<<diffusionConst<<","<<b<<","<<dimension<<endl;
    
     float sigma = Variance(monomer1,monomer2,connectivityFraction,numMonomers,b);
            
      float mfet = ((pow(2*PI,float(dimension)) /(4.0*PI*diffusionConst*encounterDist*pow(2.0*float(dimension)*PI*diffusionConst,float(dimension)/2))))*pow(sigma,float(dimension)/2);
      
      return mfet;
}

// Generate polymer graph laplacian (general connectivity)
MatrixXf GraphLaplacian(int numMonomers, int numConnectors, bool normalTransform)
{

    // generate a general connectivity matrix (graph laplacian)
    // if normalTransform ==true the graph laplacian is the averag connectivity matrix
    // otherwise is is a specific connectivity matrix with random numConnectors connections between non nearest neighboring monomers.
    MatrixXf L = RouseMatrix(numMonomers);
    float xi   = 2.0*float(numConnectors)/((float(numMonomers)-1.0)*(float(numMonomers)-2.0));
                    
    int randPosRow = 0;
    int randPosCol = 0;
    bool runFlag   = true;
   
    if (normalTransform==false) {

        for (int cIdx=0; cIdx<numConnectors; cIdx++) {
            runFlag = true;
            srand(time(NULL)+rand()*rand());
            while (runFlag==true) {
               
                randPosRow = rand() %numMonomers;
                randPosCol = rand() %numMonomers;

                if (abs(randPosCol-randPosRow)>1) {
                    if (L(randPosRow,randPosCol)>-1.0) {
                        L(randPosRow,randPosCol) = -1.0;
                        L(randPosCol,randPosRow) = -1.0;
                        L(randPosCol,randPosCol)+=  1.0;// increase diagonal by 1
                        L(randPosRow,randPosRow)+=  1.0;// increase diagonal by 1
                        runFlag = false;// exit the loop
                    }
                }
            }
        }
		// add fixed connectors 
		for (int mIdx=0; mIdx<params.fixedConnectors.rows(); mIdx++){
			 if (L(params.fixedConnectors(mIdx,0),params.fixedConnectors(mIdx,1))==0.0)
			 {
			  L(params.fixedConnectors(mIdx,0),params.fixedConnectors(mIdx,1))=-1.0;
			  L(params.fixedConnectors(mIdx,1),params.fixedConnectors(mIdx,0))=-1.0;
			  L(params.fixedConnectors(mIdx,1),params.fixedConnectors(mIdx,1))+=1.0;
			  L(params.fixedConnectors(mIdx,0),params.fixedConnectors(mIdx,0))+=1.0;
			 }
		}
    } else if (normalTransform==true) {
        // Return the diagonalized average connectivity matrix

        MatrixXf rclVals = EigenValues(numMonomers,xi);       
        // fill the matrix with xi
        for (int mIdx=0; mIdx<L.rows(); mIdx++ ) {
            for (int nIdx=0; nIdx<L.cols(); nIdx++) {
                L(mIdx,nIdx)=0.0;// zero out values of the matrix 
            }            
            L(mIdx,mIdx)= rclVals(mIdx);
        }
    }
	
    laplacian = L;
    return L;
}

// the RCL eigenvalues, with no fixed connectors
MatrixXf EigenValues(int numMonomers, float xi)
{
    MatrixXf rclEigenVals =MatrixXf::Constant(numMonomers,1,0.0);
    for (int mIdx=1; mIdx<numMonomers; mIdx++) {
        rclEigenVals(mIdx,0) = float(numMonomers)*xi+4.0*(1.0-xi)*pow(sin(float(mIdx)*PI/(2.0*float(numMonomers))),2.0);
    }
    return rclEigenVals;
}

// Generate a Rouse matrix (Laplacian)

MatrixXf RouseMatrix(int numMonomers)
{
    MatrixXf rouseMat=MatrixXf::Constant(numMonomers,numMonomers,0.0);
    // make sure it is all zeros
    for (int mIdx=0; mIdx<rouseMat.rows(); mIdx++) {
        for (int nIdx=0; nIdx<rouseMat.cols(); nIdx++) {
            rouseMat(mIdx,nIdx)=0.0;
        }
    }


    for (int i=0; i<numMonomers; i++) {
        rouseMat(i,i)=2.0;
    }
    // super and sub diagonals
    for (int i=0; i<numMonomers-1; i++) {
        rouseMat(i,i+1)=-1.0;
    }
    for (int i=1; i<numMonomers; i++) {
        rouseMat(i,i-1)=-1.0;
    }
    rouseMat(0,0)=1.0;
    rouseMat(numMonomers-1,numMonomers-1)=1.0;

    return rouseMat;
}

// the rouse polymer's eigenvectors
MatrixXf RouseEigenVectors(int numMonomers)
{
    // Input numMonomers as a vectors [N1,N2,..Nt] of monomers in each segment, the reuslt is a block matrix of
    // Rouse eigen vectors coresponding to each Ni in block diagonal matrices, by the order Ni appears    
    
    
//    // --------------------------------------
    MatrixXf rouseEigenVec = MatrixXf::Constant(numMonomers,numMonomers,0.0);    
    for (int mIdx =1; mIdx<numMonomers; mIdx++) {
        for (int pIdx =0; pIdx<numMonomers; pIdx++) {
            rouseEigenVec(mIdx,pIdx)= sqrt(2/float(numMonomers))*cos(pIdx*PI*(float(mIdx)-0.5)/float(numMonomers));
        }
    }
    // fill first row
    for (int pIdx=0; pIdx<numMonomers; pIdx++) {
        rouseEigenVec(0,pIdx)= sqrt(1/float(numMonomers));
    }
    return rouseEigenVec;
}
//simulation related function

int Step()
{
     
//    MatrixXf noise = Noise(pos.rows(), pos.cols(),D,dt);
    stepNum+=1;
    curPos.noalias() +=  laplacian* ((-(1.0*params.dimension)*params.D *params.dt / (params.b*params.b)) * curPos) +
                Noise(params.numMonomers, params.dimension,params.D,params.dt);//+ noise;


//	prevPos = curPos;
    return 0;
}

int DisconnectMonomers(int monomer1, int monomer2)
{
    // Does not work for average laplacian matrix 
	
    if ((laplacian(monomer1,monomer2)!=0) && (monomer1!=monomer2))
    {
     
      laplacian(monomer1,monomer2)  = 0;// disconnet 
      laplacian(monomer2,monomer1)  = 0;// disconnect
      laplacian(monomer1,monomer1)-= 1;// reduce diagonal count by 1
      laplacian(monomer2,monomer2)-= 1;// reduce diagonal count by 1
     
//      connectivityMat(monomer1,monomer2)=0; //update connectivitymatrix 
//      connectivityMat(monomer2,monomer1)=0;
      numConnectedMonomers-=1;      // decrease counter by 1;
	
    }
    
    return 0;
}

int ConnectMonomers(int monomer1, int monomer2)
{
    // Does not work for average laplacian matrix 
    if ((laplacian(monomer1,monomer2)!=0) && (monomer1 !=monomer2))
    {
        laplacian(monomer1,monomer2) =-1;
        laplacian(monomer2,monomer1) =-1;
        laplacian(monomer1,monomer1)+= 1;
        laplacian(monomer2,monomer2)+= 1;
//        connectivityMat(monomer1,monomer2)=1;//update connectivityMAt
//        connectivityMat(monomer2,monomer1)=1;
        numConnectedMonomers+=1;// increase counter by 1
    }
    return 1;
}

/** Generate random noise
 * @brief generate Normaly distributed random noise
 * @param numMonomers number of monomers 
 * @param dimension dimension of the data
 * @param diffusionConst difusion constant
 * @params dt simulation time step 
 * @return noise matrixXf
 */
MatrixXf Noise(int numMonomers, int dimension, float diffusionConst, float dt)//float mu, float sigma, int numMonomers, int dimension)
{
    // Generate noise with mean mu and std sigma/dimension
    MatrixXf vecOut = MatrixXf::Constant(numMonomers,dimension, 0.0);
    for(int m = 0; m < numMonomers; m++) {
        for(int di = 0; di < dimension; di++) {
            vecOut(m, di) = sqrt(2.0*diffusionConst*dt)*nDist(gen);
        }
    }
	
    return vecOut;
}

MatrixXf GetConnectedMonomers(string state)
{
    // Find all non nearest neighboring monomers connected based on the Laplacian of the graph matrix     
   
    connectedMonomers=MatrixXf::Constant(numConnectedMonomers,2,0);
      
    int nextInd = 0;// running index
    for (int mIdx=0; mIdx<(params.numMonomers-1); mIdx++)
    {
        for (int nIdx=(mIdx+1); nIdx<params.numMonomers; nIdx++)
        {         
                        
            if (laplacian(mIdx,nIdx)<0)//  if there is a connection
            {           
                connectedMonomers(nextInd,0) = mIdx;// add row and column to the list
                connectedMonomers(nextInd,1) = nIdx;             
                nextInd                      += 1;                
            }
        }
    }
    
    return connectedMonomers;
}

MatrixXf GetCenterOfMass()
{
	centerOfMass = curPos.colwise().mean();// record center of mass 
	return centerOfMass;
}

float ComputeSquareRadiusOfGyration(){
	// TODO move function to RandomlyCrosslinkedPolymer
    // Compute the square radius of gyration
	MatrixXf pos = curPos;
    if(params.normalTransform == true) {
        MatrixXf rv = RouseEigenVectors(params.numMonomers);
        pos = rv.transpose() * curPos; // transform back to spatial coordinates
    }

    GetCenterOfMass(); // center of mass    
    MatrixXf tempRowSrg = MatrixXf::Constant(params.numMonomers, 1, 0.0);

    for(int mIdx = 0; mIdx < params.numMonomers; mIdx++) {
        for(int dIdx = 0; dIdx < params.dimension; dIdx++) {
            tempRowSrg(mIdx) += pow(pos(mIdx, dIdx) - centerOfMass(0, dIdx), 2.0);
        }
    }
    srg = (tempRowSrg.colwise().sum() / float(params.numMonomers))(0);

    return srg;
}

};// end public


